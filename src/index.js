"use strict";

const joinProgram = document.querySelector(".join-program");
const emailInput = document.getElementById("input-email");
const form = document.getElementById("form");

const showSection = () => {
  joinProgram.style.display = "block";
};

const handleSubmit = (e) => {
  e.preventDefault();

  const userInput = emailInput.value;
  console.log(userInput);

  emailInput.value = "";
};

window.addEventListener("load", showSection);
form.addEventListener("submit", handleSubmit);
